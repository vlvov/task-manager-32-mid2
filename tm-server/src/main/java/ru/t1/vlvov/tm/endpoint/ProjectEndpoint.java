package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.vlvov.tm.api.service.IServiceLocator;
import ru.t1.vlvov.tm.dto.Request.*;
import ru.t1.vlvov.tm.dto.Response.*;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String statusValue = request.getStatus();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull Project project = serviceLocator.getProjectService().changeProjectStatusById(userId, projectId, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex() - 1;
        @Nullable final String statusValue = request.getStatus();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull Project project = serviceLocator.getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) {
        check(request);
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull Project project = serviceLocator.getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    public ProjectListResponse listProject(@NotNull ProjectListRequest request) {
        check(request);
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeById(@NotNull ProjectRemoveByIdRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull Project project = serviceLocator.getProjectService().removeById(userId, projectId);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex() - 1;
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull Project project = serviceLocator.getProjectService().removeByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateById(@NotNull ProjectUpdateByIdRequest request) {
        check(request);
        @Nullable final String id = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull Project project = serviceLocator.getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        check(request);
        @Nullable final Integer index = request.getIndex() - 1;
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull Project project = serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
