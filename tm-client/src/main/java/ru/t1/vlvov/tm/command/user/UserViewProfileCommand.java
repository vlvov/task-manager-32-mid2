package ru.t1.vlvov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-view-profile";

    @NotNull
    private final String DESCRIPTION = "Display user profile.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        @Nullable final User user = getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        showUser(user);
    }

}