package ru.t1.vlvov.tm.client;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.Application;
import ru.t1.vlvov.tm.api.client.IEndpointClient;
import ru.t1.vlvov.tm.dto.Request.AbstractRequest;
import ru.t1.vlvov.tm.dto.Response.AbstractResponse;
import ru.t1.vlvov.tm.dto.Response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 2211;

    @NotNull
    private Socket socket;

    public AbstractEndpointClient() {
    }

    public AbstractEndpointClient(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @NotNull
    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    @NotNull
    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    @Override
    public Object call(@NotNull final Object object) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(object);
        return getObjectInputStream().readObject();
    }

    @Nullable
    @Override
    public Socket connect() throws IOException {
        socket = new Socket(host, port);
        return socket;
    }

    @Nullable
    @Override
    public Socket disconnect() throws IOException {
        if (socket != null) socket.close();
        return socket;
    }

    @NotNull
    @Override
    public <T> T call(@Nullable final AbstractRequest request, @NotNull Class<T> classResponse) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(request);
        @NotNull final Object response = getObjectInputStream().readObject();
        if (response instanceof ApplicationErrorResponse) {
            @NotNull ApplicationErrorResponse errorResponse = (ApplicationErrorResponse) response;
            throw new RuntimeException(errorResponse.getMessage());
        }
        return (T) response;
    }

}
