package ru.t1.vlvov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.endpoint.IUserEndpoint;
import ru.t1.vlvov.tm.dto.Request.*;
import ru.t1.vlvov.tm.dto.Response.*;

@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @SneakyThrows
    UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @SneakyThrows
    UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @NotNull
    @SneakyThrows
    UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

}
