package ru.t1.vlvov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.client.IEndpointClient;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.enumerated.Role;

import java.net.Socket;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    private final String DESCRIPTION = "Disconnect from server.";

    @NotNull
    private final String NAME = "disconnect";

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @SneakyThrows
    public void execute() {
        serviceLocator.getConnectionEndpointClient().disconnect();
    }

}
