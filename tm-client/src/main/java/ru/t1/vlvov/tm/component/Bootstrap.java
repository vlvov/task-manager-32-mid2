package ru.t1.vlvov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.vlvov.tm.api.client.IEndpointClient;
import ru.t1.vlvov.tm.api.repository.ICommandRepository;
import ru.t1.vlvov.tm.api.service.*;
import ru.t1.vlvov.tm.client.*;
import ru.t1.vlvov.tm.client.UserEndpointClient;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.exception.AbstractException;
import ru.t1.vlvov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.vlvov.tm.exception.system.CommandNotSupportedException;
import ru.t1.vlvov.tm.repository.CommandRepository;
import ru.t1.vlvov.tm.service.*;
import ru.t1.vlvov.tm.util.SystemUtil;
import ru.t1.vlvov.tm.util.TerminalUtil;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Modifier;
import java.util.Set;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.vlvov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final IEndpointClient endpointClient = new ConnectionEndpointClient();

    @NotNull
    private final DomainEndpointClient domainEndpointClient = new DomainEndpointClient();

    @NotNull
    private final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient();

    @NotNull
    private final SystemEndpointClient systemEndpointClient = new SystemEndpointClient();

    @NotNull
    private final TaskEndpointClient taskEndpointClient = new TaskEndpointClient();

    @NotNull
    private final UserEndpointClient userEndpointClient = new UserEndpointClient();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        final long pid = SystemUtil.getPID();
        File file = new File(fileName);
        FileWriter myWriter = new FileWriter(file);
        myWriter.write(Long.toString(pid));
        myWriter.close();
        file.deleteOnExit();
    }

    private void initDemoData() {

    }

    private void prepareStartup() {
        initPID();
        loggerService.info("**WELCOME TO TASK-MANAGER**");
        fileScanner.start();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
        fileScanner.stop();
    }

    public void run(@NotNull final String[] args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        processCommands();
    }

    private boolean processArguments(@Nullable final String args[]) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        if (argument == null) return false;
        processArgument(argument);
        return true;
    }

    private void processArgument(@NotNull final String argument) {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException(argument);
        command.execute();
    }

    private void processCommands() {
        String command;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                loggerService.command(command);
                processCommand(command);
                System.out.println("OK");
            } catch (AbstractException e) {
                loggerService.error(e);
                System.err.println("FAIL");
            }
        }
    }

    private void processCommand(final String name) {
        processCommand(name, true);
    }

    public void processCommand(final String name, final boolean checkRole) {
        @Nullable final AbstractCommand command = commandService.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException(name);
        if (checkRole) getAuthService().checkRoles(command.getRoles());
        command.execute();
    }

}
