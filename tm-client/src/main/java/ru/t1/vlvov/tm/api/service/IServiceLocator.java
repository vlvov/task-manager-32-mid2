package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.client.IEndpointClient;
import ru.t1.vlvov.tm.client.*;

public interface IServiceLocator {

    @Nullable
    ICommandService getCommandService();

    @Nullable
    ILoggerService getLoggerService();

    @NotNull
    IProjectService getProjectService();

    @Nullable
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IUserService getUserService();

    @Nullable
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    DomainEndpointClient getDomainEndpointClient();

    @NotNull
    ProjectEndpointClient getProjectEndpointClient();

    @NotNull
    TaskEndpointClient getTaskEndpointClient();

    @NotNull
    UserEndpointClient getUserEndpointClient();

    @NotNull
    SystemEndpointClient getSystemEndpointClient();


}