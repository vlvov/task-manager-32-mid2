package ru.t1.vlvov.tm.dto.Response;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.Project;

public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable Project project) {
        super(project);
    }

}
