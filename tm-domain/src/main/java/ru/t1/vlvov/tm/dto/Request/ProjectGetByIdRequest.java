package ru.t1.vlvov.tm.dto.Request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

}
