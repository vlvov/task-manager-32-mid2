package ru.t1.vlvov.tm.dto.Response;

import ru.t1.vlvov.tm.model.User;

public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse() {

    }

    public UserProfileResponse(User user) {
        super(user);
    }

}
