package ru.t1.vlvov.tm.dto.Request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer taskIndex;

    @Nullable
    private String status;

}
